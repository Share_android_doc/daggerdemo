package com.example.kimsoerhrd.daggerdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.kimsoerhrd.daggerdemo.app.MyApplication;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    String apiUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((MyApplication)getApplication()).getApplicationComponent().inject(this);
        Log.e("oooo",apiUrl);
    }
}
