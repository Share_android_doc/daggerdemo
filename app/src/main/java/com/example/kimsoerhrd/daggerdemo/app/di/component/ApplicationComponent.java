
package com.example.kimsoerhrd.daggerdemo.app.di.component;
import com.example.kimsoerhrd.daggerdemo.LoginActivity;
import com.example.kimsoerhrd.daggerdemo.MainActivity;
import com.example.kimsoerhrd.daggerdemo.app.di.module.ApplicationModule;
import com.example.kimsoerhrd.daggerdemo.app.di.module.ConstantModule;

import dagger.Component;

@Component(modules = {ConstantModule.class, ApplicationModule.class})
public interface ApplicationComponent {
        void inject(MainActivity activity);
        void inject(LoginActivity activity);
}
