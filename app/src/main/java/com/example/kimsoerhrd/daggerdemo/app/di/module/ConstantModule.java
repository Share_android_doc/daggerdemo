package com.example.kimsoerhrd.daggerdemo.app.di.module;



import com.example.kimsoerhrd.daggerdemo.app.di.qualifier.ApiKey;

import dagger.Module;
import dagger.Provides;

@Module
public class ConstantModule {

    @Provides
    public String provideApiUrl(){
        return "https://www.google.com";
    }
    @Provides
    //@Named("api_key")
    @ApiKey
    public String provideApiKey(){
        return "key:adsj;aflkasjflkjasf";
    }
}
