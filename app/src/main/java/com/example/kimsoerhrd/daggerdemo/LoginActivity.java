package com.example.kimsoerhrd.daggerdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.kimsoerhrd.daggerdemo.app.MyApplication;
import com.example.kimsoerhrd.daggerdemo.app.di.qualifier.ApiKey;

import javax.inject.Inject;

public class LoginActivity extends AppCompatActivity {


    @Inject
    String apiUrl;

    @Inject
    //@Named("api_key")
    @ApiKey
    String apiKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ((MyApplication)getApplication()).getApplicationComponent().inject(LoginActivity.this);
            Log.e("oooo", apiUrl+apiKey);
    }
}
