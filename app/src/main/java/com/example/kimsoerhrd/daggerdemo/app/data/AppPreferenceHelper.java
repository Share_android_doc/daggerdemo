package com.example.kimsoerhrd.daggerdemo.app.data;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class AppPreferenceHelper implements PreferenceHelper {

    private static final String USER_PREFERENCE = "user_preference";
    private static final String USER_ID = "user_id";
    private static final String USER_NAME = "user_name";

    private SharedPreferences sharedPreferences;

    @Inject
    public AppPreferenceHelper(Context context) {
        this.sharedPreferences = context.getSharedPreferences(USER_PREFERENCE, Context.MODE_PRIVATE);
    }

    @Override
    public void setUserId() {

    }

    @Override
    public void getUserId() {

    }

    @Override
    public void setUsername() {

    }

    @Override
    public void getUsername() {

    }
}
