package com.example.kimsoerhrd.daggerdemo.app;

import android.app.Application;

import com.example.kimsoerhrd.daggerdemo.app.di.component.ApplicationComponent;
import com.example.kimsoerhrd.daggerdemo.app.di.module.ConstantModule;
import com.example.kimsoerhrd.daggerdemo.app.di.DaggerApplicationComponent;

public class MyApplication extends Application {

    private ApplicationComponent applicationComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .constantModule(new ConstantModule())
                .build();
    }
    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
