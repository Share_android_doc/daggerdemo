package com.example.kimsoerhrd.daggerdemo.app.data;

public interface PreferenceHelper {
    void setUserId();
    void getUserId();

    void setUsername();
    void getUsername();

}
